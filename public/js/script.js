$(document).ready(function() {
	// Get current value that's displayed (initialization)
	var breakLength = +$(".break-length .num").text();
	var sessionLength = +$(".session-length .num").text();
	var timeLeft = 25 * 60;
	var isRunning = false;
	var isBreak = false;

	// Update on increasing/decreasing value
	$(".break-length .increase").click(function() {
		pause();
		$(".break-length .num").text(breakLength + 1);
		breakLength++;
		if (isBreak) {
			$(".clock .num").text(breakLength + ":00");
			timeLeft = breakLength * 60;
		}
	});

	$(".session-length .increase").click(function() {
		pause();
		$(".session-length .num").text(sessionLength + 1);
		sessionLength++;
		if (!isBreak) {
			$(".clock .num").text(sessionLength + ":00");
			timeLeft = sessionLength * 60;
		}
	});

	$(".break-length .decrease").click(function() {
		pause();
		// We don't want 0 or negative values
		if (breakLength > 1) {
			$(".break-length .num").text(breakLength - 1);
			breakLength--;
			if (isBreak) {
				$(".clock .num").text(breakLength + ":00");
				timeLeft = breakLength * 60;
			}
		}
	});

	$(".session-length .decrease").click(function() {
		pause();
		if (sessionLength > 1) {
			$(".session-length .num").text(sessionLength - 1);
			sessionLength--;
			if (!isBreak) {
				$(".clock .num").text(sessionLength + ":00");
				timeLeft = sessionLength * 60;
			}
		}
	});

	// Pause countdown on click
	$(".clock").click(function() {
		if (isRunning) {
			pause();
		}
		else {
			unPause();
		}
	});

	$(".pause").click(function() {
		if (isRunning) {
			pause();
		}
		else {
			unPause();
		}
	});

	// Reset button's behaviour
	$('.reset').click(function() {
		pause();
		$(".clock .num").text(sessionLength + ":00");
		timeLeft = sessionLength * 60;
	});

	// Functions to actually pause (to somewhat reduce code)
	function unPause() {
		isRunning = true;
		$(".clock").css("border-color", "#084aff");

		var refreshIntervalId = setInterval(function() {
			// Switch between sessions and breaks and play sound when time's up
			if (timeLeft === 1) {
				// Are we having a session or a break?
				if (!isBreak) {
					isBreak = true;
					$(".clock .label").text("Break");
					timeLeft = breakLength * 60 + 1;
				}
				else {
					isBreak = false;
					$(".clock .label").text("Session");
					timeLeft = sessionLength * 60 + 1;
				}
				var audio = new Audio('sound/bell.oga');
				audio.play();
			}

			if (isRunning) {
				timeLeft--;
			}
			// Stop current refresh interval after we clicked on pause
			else {
				clearInterval(refreshIntervalId);
			}

			// Work with seconds and translate it into mm:ss format
			var minutes = Math.floor(timeLeft / 60);
			var seconds = timeLeft % 60;
			if (seconds.toString().length == 1) {
				$(".clock .num").text(minutes + ":0" + seconds);
			}
			else {
				$(".clock .num").text(minutes + ":" + seconds);
			}
		}, 1000);
	}

	function pause() {
		isRunning = false;
		$(".clock").css("border-color", "#ff0c55");
	}
});
